# LabPager #



### What is LabPager? ###

LabPager is an integration of PagerDuty with LabAlert, a laboratory monitoring system.

Customizable dashboards from PagerDuty and LabAlert are integrated to allow incident alerts and status notifications 
pertaining to lab processes such as temperature, humidity, and carbon dioxide deviation.

